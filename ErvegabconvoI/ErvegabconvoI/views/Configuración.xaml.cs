﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ErvegabconvoI.models;

namespace ErvegabconvoI.views
{
    /// <summary>
    /// Interaction logic for Configuración.xaml
    /// </summary>
    public partial class Configuración : Window
    {
        public Configuración()
        {
            InitializeComponent();
            SetupController();
        }

        public persona GetData()
        {
            persona person = new persona
            {
                nombre = NombreTextBox.Text,
                correo = CorreoTextBox.Text,
            };
            return person;
        }

        public void SetData(persona data)
        {
            NombreTextBox.DataContext = data;
            CorreoTextBox.DataContext = data;
        }

        protected void SetupController()
        {
            
        } 


    }
}
